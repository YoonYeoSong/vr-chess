﻿using UnityEngine;
using System.Collections;

public class kit : MonoBehaviour {


    int count = 0;
    RaycastHit hit; //레이케스트
    GameObject chessboard,ray; // 체스 판
    string parentName;  //plyaer이름 확인을 위해서
    
	playercontrol GameMaster;
    // Use this for initialization
    void Start () {


        ray = Resources.Load("ray") as GameObject;
        chessboard = GameObject.Find("chessboard");
		GameMaster = GameObject.Find ("gameMaster").GetComponent <playercontrol>();
     
    }
	
	// Update is called once per frame
	void Update ()
    {
        grab();
        
    }

   void grab()
    {
        if (count == 0)
        {
            if (this.GetComponent<GrabbableObject>().IsGrabbed())
            {
                GameObject goTemp = Instantiate(ray, transform.position + new Vector3(0, -5, 0), Quaternion.identity) as GameObject;
                goTemp.transform.parent = transform;

                parentName = this.transform.parent.parent.name;

                chesschoice();
           
                StartCoroutine(delay());

                count++;
            }
         

        }


        else if(count ==1)
        {
            Physics.Raycast(this.transform.position, this.transform.up, out hit, 10.0f);
            if (this.GetComponent<GrabbableObject>().IsGrabbed()==false)
            {
				this.GetComponent<Rigidbody>().velocity = Vector3.zero;
               
				if (hit.transform.name == "bluepan(Clone)") {
                    

					hit.transform.GetComponent<bluepan> ().OnMouseDown (); 
					this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
				} else if (hit.transform.name == "redpan(Clone)") {
                    
					hit.transform.GetComponent<redpan> ().OnMouseDown ();
					this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
				} else {
					this.transform.position = this.transform.parent.position;
					this.GetComponent<Rigidbody>().velocity = Vector3.zero;
					
				}
          
                
				this.transform.position = this.transform.parent.position + new Vector3(0, 1.5f, 0);
                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
				box_on ();
                count = 0;
                Destroy(GameObject.Find("ray(Clone)"));
                Destroy(GameObject.Find("Mouse(Clone)"));

                StartCoroutine(delay2());


               


            }
        }
    }


    void chesschoice()  
    {

        if (this.transform.parent.name.Substring(0, 4) == "rook")
        {
            this.transform.parent.GetComponent<rook>().OnMouseDown();
        }
        else if (this.transform.parent.name.Substring(0, 4) == "pawn")
        {
            this.transform.parent.GetComponent<pawn>().OnMouseDown();
        }
        else if (this.transform.parent.name.Substring(0, 4) == "king")
        {
            this.transform.parent.GetComponent<King>().OnMouseDown();
        }
        else if (this.transform.parent.name.Substring(0, 4) == "bish")
        {
            this.transform.parent.GetComponent<bishop>().OnMouseDown();
        }
        else if (this.transform.parent.name.Substring(0, 4) == "knig")
        {
            this.transform.parent.GetComponent<Knight>().OnMouseDown();
        }
        else if (this.transform.parent.name.Substring(0, 4) == "quee")
        {
            this.transform.parent.GetComponent<queen>().OnMouseDown();
        }
    }


    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.3f);
		box_off ();
     
        
    }

    IEnumerator delay2()
    {
       
        
        yield return new WaitForSeconds(0.5f);
        chessboard.GetComponent<PanDestroy>().routeDel();
      
    }
  
	void box_on()
	{
		foreach (Transform T in GameMaster.play2.GetComponentInChildren<Transform>())
		{
			T.GetComponent<BoxCollider> ().enabled = true;
		}
	}

	void box_off()
	{
		foreach (Transform T in GameMaster.play2.GetComponentInChildren<Transform>())
		{
		
			T.GetComponent<BoxCollider> ().enabled = false;
		}
	}

}
