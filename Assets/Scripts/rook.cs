﻿using UnityEngine;
using System.Collections;

public class rook : MonoBehaviour
{
    public GameObject R1;
    public GameObject R2;
    public GameObject R3;
    public GameObject R4;

    GameObject bluepan, mouse;
    GameObject chessboard;
    // Use this for initialization
    void Start()
    {
        this.bluepan = Resources.Load("bluepan") as GameObject;
        
        chessboard = GameObject.Find("chessboard");
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnMouseDown() {
        StartCoroutine(Route());
    }
    public void AAA()
    { StartCoroutine(Route()); }
    IEnumerator Route()
    {
        bluepan.GetComponent<bluepan>().GameOB(this.gameObject);
        yield return new WaitForSeconds(0.01f);
        chessboard.GetComponent<PanDestroy>().routeDel();
        yield return new WaitForSeconds(0.01f);

        RaycastHit hit;
        
        //front
        if (Physics.Raycast(R1.transform.position, R1.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.z - transform.position.z;
            for (int i = 1; i <= frontPos; i++)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
            }

           // if (hit.collider.tag == "Enemy")
          //  {
          //      Instantiate(this.red, transform.position + new Vector3(0, -0.5f, frontPos), Quaternion.Euler(new Vector3(0, 0, 0)));
          //  }
        }

        //back
        if (Physics.Raycast(R2.transform.position, R2.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.z - transform.position.z;
            for (int i = -1; i >= frontPos; i--)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
            }

           // if (hit.collider.tag == "Enemy")
           // {
          //      Instantiate(this.red, transform.position + new Vector3(0, -0.5f, frontPos), Quaternion.Euler(new Vector3(0, 0, 0)));
           // }
        }

        //right
        if (Physics.Raycast(R3.transform.position, R3.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.x - transform.position.x;
            for (int i = 1; i <= frontPos; i++)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
            }

          //  if (hit.collider.tag == "Enemy")
          //  {
          //      Instantiate(this.red, transform.position + new Vector3(frontPos, -0.5f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
           // }
        }

         //left
        if (Physics.Raycast(R4.transform.position, R4.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.x - transform.position.x;
            for (int i = -1; i >= frontPos; i--)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
            }

          //  if (hit.collider.tag == "Enemy")
           // {
           //     Instantiate(this.red, transform.position + new Vector3(frontPos, -0.5f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
           // }
        }
    }

}