﻿using UnityEngine;
using System.Collections;

public class Knight : MonoBehaviour {

   
    public GameObject bluepan;
    GameObject chessboard;
  
	// Use this for initialization
	void Start () {
        chessboard = GameObject.Find("chessboard");
    }
	
	// Update is called once per frame
	void Update () {

        

    }
    public void OnMouseDown()
    {
        StartCoroutine(Route());
    }

    IEnumerator Route()
    {
        bluepan.GetComponent<bluepan>().GameOB(this.gameObject);
        yield return new WaitForSeconds(0.01f);
        chessboard.GetComponent<PanDestroy>().routeDel();
        yield return new WaitForSeconds(0.01f);

        Instantiate(this.bluepan, transform.position + new Vector3(2, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
        
        Instantiate(this.bluepan, transform.position + new Vector3(2, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
        
        Instantiate(this.bluepan, transform.position + new Vector3(1, -0.49f, 2), Quaternion.Euler(new Vector3(0, 0, 0)));
        Instantiate(this.bluepan, transform.position + new Vector3(-1, -0.49f, 2), Quaternion.Euler(new Vector3(0, 0, 0)));
        
        Instantiate(this.bluepan, transform.position + new Vector3(-2, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
      
        Instantiate(this.bluepan, transform.position + new Vector3(-2, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
       
        Instantiate(this.bluepan, transform.position + new Vector3(1, -0.49f, -2), Quaternion.Euler(new Vector3(0, 0, 0)));
       
        Instantiate(this.bluepan, transform.position + new Vector3(-1, -0.49f, -2), Quaternion.Euler(new Vector3(0, 0, 0)));

    }

}

 
