﻿using UnityEngine;
using System.Collections;

public class raycast : MonoBehaviour {

    public static Vector3 hitPos;
    GameObject mouse;
    //GameObject Player;
    // bool click;

    void Start()
    {
        //transform.rotation = Quaternion.Euler (270f, 0, 0);
        this.mouse = Resources.Load("Mouse") as GameObject;
        hitPos = new Vector3(0, 0, 0);

        //Player = GameObject.Find("Player1");
        // click = false;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.up, out hit, 10.0f))
        {
            if (GameObject.Find("Mouse(Clone)") == null && hit.transform.tag == "pan")
            {
                Instantiate(mouse, hit.transform.position + new Vector3(0, 0.1f, 0), hit.transform.rotation);
                hitPos = hit.transform.position;

            }
            else if (GameObject.Find("Mouse(Clone)") && hit.transform.position != hitPos)
            {
                Destroy(GameObject.Find("Mouse(Clone)"));
            }
        }


    }


}
