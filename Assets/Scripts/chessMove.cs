﻿using UnityEngine;
using System.Collections;

public class chessMove : MonoBehaviour {

    #region 상수
    const int UP = 0;
    const int RIGHT = 1;
    const int DOWN = 2;
    const int LEFT = 3;

    const int QUADRANT1 = 0;
    const int QUADRANT2 = 1;
    const int QUADRANT3 = 2;
    const int QUADRANT4 = 3;

    //knight 상수
    const int UP1 = 0;
    const int UP2 = 1;
    const int UP3 = 2;
    const int UP4 = 3;
    const int DOWN1 = 4;
    const int DOWN2 = 5;
    const int DOWN3 = 6;
    const int DOWN4 = 7;

    #endregion

    public static int ChessArray;
    public static string WhiteName;
    public string WhiteN; // string 으로 화이트의 말의 이름을 정해두었다.
    public static int Pawn1random;
    //public static int PawnCount;
    //public static int pawncount;

    public int MovePieceNumber; // 우선순위를 주기위한 움직이는 수.
    public GameObject nextMovePiece; // 
    public static GameObject STnextMovePiece; // 
    GameObject PieceNumber; // 말들의 오브젝트
    Vector2 oop; // 우선순위 order of priority
    //Vector3 ChessPos;
    int PieceNumCurrentposition; // 말의 포지션
    public int direction = 0; // 룩의 방향
    public static int directionNum;
    public int staticCurrentPosition;

    int tmp; // 첫번째 위치 갱신
    int tmp2; // 두번째 위치갱신
    public bool Go = false; //이동가능
    public GameObject[] PawnArray;
    public GameObject[] KArray;
    Vector3 panposition;// 클릭한 판의 position값 
                        //public static int PR = Random.Range(0, 7);

	public GameObject CameraZoom; // 카메라 줌

    GameObject ChessBoardManager;

    ///////////////////////애니메이션
    public Animator animator;


    GameObject forcePawnW, forceRookW, forceBishopW , forceQueenW , forceKnightW, forceKingW;
    /////////////////////////

    /// <summary>
    /// 애니메이션 시작
    /// </summary>
    void AnimStr()
    {
        
        if (animator != null)
        {
            animator.SetBool("attack", true);
        }
    }
    /// <summary>
    /// 애니메이션 끝
    /// </summary>
    void AnimEnd()
    {
        if (animator != null)
        {
            animator.SetBool("attack", false);
        }
    }






    void Awake()
    {
        PieceNumCurrentposition = 0;
        directionNum = 0;
        AIObject.piecesSearchPos = false;
      
        MovePieceNumber = 0;
        WhiteName = WhiteN;
        tmp = 0; //처음 위치를 0으로 선언
        tmp2 = 0; // 마지막 위치를 0으로 선언
    }

    // Use this for initialization
    void Start() {
        
        ChessBoardManager = GameObject.FindGameObjectWithTag("ChessBoard");
        forcePawnW = Resources.Load("forcePawnW") as GameObject;
        
        forceRookW = Resources.Load("forceRookW") as GameObject;
       
        forceBishopW = Resources.Load("forceBishopW") as GameObject;
		forceQueenW = Resources.Load("forceQueenW") as GameObject;
		forceKnightW = Resources.Load("forceKnightW") as GameObject;
		forceKingW = Resources.Load("forceKingW") as GameObject;

		CameraZoom = GameObject.Find ("MainCamera");
        
    }

    // Update is called once per frame
    void Update()
    {

        // 화이트 이동
        if (Go == true)
        {
            int Y = 0;
            if (transform.position.x < panposition.x)
            {
                this.transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
                if (transform.position.x > panposition.x)
                {
                   
                    Go = false;
                    this.transform.position = panposition;
                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                    StartCoroutine(delay());
					CameraZoom.GetComponent<CameraPR> ().Rtzoom ();
                }
            }
            else if (transform.position.x > panposition.x)
            {
                this.transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
                if (transform.position.x < panposition.x)
                {
                    
                    Go = false;
                    this.transform.position = panposition;

                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
					StartCoroutine(delay());
					CameraZoom.GetComponent<CameraPR> ().Rtzoom ();
                }
            }
            else if (transform.position.z < panposition.z)
            {
                this.transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
                if (transform.position.z > panposition.z)
                {
                   
                    Go = false;
                    this.transform.position = panposition;

                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                    StartCoroutine(delay());
					CameraZoom.GetComponent<CameraPR> ().Rtzoom ();
                }
            }
            else if (transform.position.z > panposition.z)
            {
                this.transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
                if (transform.position.z < panposition.z)
                {
                   
                    Go = false;
                    this.transform.position = panposition;

                    this.transform.rotation = Quaternion.Euler(0, 0, 0);
                    StartCoroutine(delay());
					CameraZoom.GetComponent<CameraPR> ().Rtzoom ();
                }
            }
           // this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }

    }

    /* 2번 게임의 보드판의 위치를 알려준후  화이트 말을 클릭하여 움직이게 된다. 움직이는 함수는 Move() 와 Attack() 두 함수가 있다.
       Move()함수와 Attack()함수의 차이점은 애니메이션이 있는지 없는지의 차이이다. 체스말 앞에 적이 없다면 그냥 일반적인 말의 이동이 진행될 것이다. 
       그 일반적 움직임의 함수가 Move() 함수이다. 만약 앞에 적이 있다면 Attack() 함수로 적을 죽이는 애니메이션과 함께 이동할것이다.

       그리고 두 함수 안에 공통적으로 들어가 있는것은 firstcalcPositionByXZ() 함수와 endcalcPositonByXZ() 두함수이다. 두함수의 이용은 아까 맨처음에
       체스 판의 배열에 각 각 말들의 위치값을 넣어줬을 것이다. 화이트 말들이 움직일때마다 그 배열의 위치값을 바꿔줘야 하므로, 체스판의 배열을 실시간으로
       컴퓨터에게 알려주는 역할을 한다.firstcalcPositionByXZ() ,endcalcPositonByXZ() 이 두함수의 매개변수는 그 해당 오브젝트의 x와z값을 가져온다. x와 z값을 가져와
       좌표값의 쓰이는 y*8 + x 라는 계산식으로 대입하여 이 말의 firstcalcPositionByXZ() 함수를 이용해 처음위치의 배열 값에 null 값을 넣어주고 이동한 위치 값 endcalcPositonByXZ()
       함수를 이용해 예를들어 pawn이 움직였으면 그 앞에 위치한 배열 값에 pawn 이라고 스트링으로 채워주는 방식이다.
    
        
            
    */


    public void Move(float x, float z)//pan에서 클릭시 Move함수 호출 x,z좌표를 매개변수로 건내준다 
    {

        panposition = new Vector3(x, 0, z);
        //방향만 틀어준다.
        var relative = transform.InverseTransformPoint(panposition);
        var angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg;
        Debug.Log(angle);
        if (transform.parent.tag == "Player2") // && transform.name == "pawn" || transform.name == "knight")
        {
            angle = angle + 180;
        }
        transform.rotation = Quaternion.Euler(0, angle, 0);

        
               
        tmp = firstcalcPositionByXZ(this.transform.position.x, this.transform.position.z); // 처음 위치를 (이 물체의 포지션의 x , 이 물체의 포지션 y )값을 가져온다.                                                                                          //  print(ChessBoard.chessBoard);
        ChessBoard.chessBoard[tmp] = ""; // 체스보드 배열의 tmp 첫번째 있던 자리에 널값을 넣어준다.
        tmp2 = endcalcPositionByXZ(panposition.x, panposition.z); // 마지막 위치에 (도착 판의 x 위치값 , 도착 판의 z 위치값)을 받는다.
        ChessBoard.chessBoard[tmp2] = "W" + WhiteN; // tmp2 마지막 위치 값을 배열에 넣어 그 물체의 string Name을 넣어준다.
        Go = true;
 
      

    }
    /// <summary>
    /// Attack 함수는 적이 있을경우 delayanim() 애니메이션 발동!
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    public void Attack(float x, float z)//pan에서 클릭시 Move함수 호출 x,z좌표를 매개변수로 건내준다 
    {
        panposition = new Vector3(x, 0, z);
        //방향만 틀어준다.
		CameraZoom.GetComponent<CameraPR> ().zoom (transform.position);
        var relative = transform.InverseTransformPoint(panposition);
        var angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg;


        Debug.Log(angle);
        if (transform.parent.tag == "Player2") // && transform.name == "pawn" || transform.name == "knight")
        {
            angle = angle + 180;
        }
        transform.rotation = Quaternion.Euler(0, angle, 0);



        tmp = firstcalcPositionByXZ(this.transform.position.x, this.transform.position.z); // 처음 위치를 (이 물체의 포지션의 x , 이 물체의 포지션 y )값을 가져온다.                                                                                          //  print(ChessBoard.chessBoard);
        ChessBoard.chessBoard[tmp] = ""; // 체스보드 배열의 tmp 첫번째 있던 자리에 널값을 넣어준다.
        tmp2 = endcalcPositionByXZ(panposition.x, panposition.z); // 마지막 위치에 (도착 판의 x 위치값 , 도착 판의 z 위치값)을 받는다.
        ChessBoard.chessBoard[tmp2] = "W" + WhiteN; // tmp2 마지막 위치 값을 배열에 넣어 그 물체의 string Name을 넣어준다.

        StartCoroutine(delayAnim());


    }
    /// <summary>
    /// 마지막 판의 위치 panposition x 값과 z 값을 계산하여 tmp2에 값을 넣어 그값을 위치 배열에 값을 넣는다.
    /// </summary>
    /// <param name="x"> panposition x 의 값</param>
    /// <param name="z"> panposition y 의 값</param>
    /// <returns></returns>
    int endcalcPositionByXZ(float x, float z)
    {
        float endZ = (z * 8) + 28.0f;
        float endX = x + 3.5f;
        int end = (int)endZ + (int)endX;
        return end;
    }

    /// <summary>
    /// 처음 Object의 위치값을 가져 와서 계산 값을 tmp에 담고 그 값을 배열에 값을 넣는다.
    /// </summary>
    /// <param name="x">오브젝트의 x값</param>
    /// <param name="z">오브젝트의 y값</param>
    /// <returns></returns>
    int firstcalcPositionByXZ(float x, float z)
    {
        float firstZ = (z * 8) + 28.0f;
        float firstX = x + 3.5f;
        int first = (int)firstZ + (int)firstX;
        return first;
    }

    public IEnumerator delay()//이동시 딜레이를 주기위하여 
    {
        yield return new WaitForSeconds(1);
        this.transform.position = panposition;
        //Go = false;
        
        //검은색 말 이동 요청
        //랜덤 이동 함수를 호출한다. 만약 선택된 기물이 이동할 수 없으면, while문을 다시 돌려서 다른 말을 선택
        int whiletmp = 0;
        


        /*
        3번
        화이트 말의 움직임 때부터 종료 시점까지 컴퓨터 블랙 말은 탐색을 시작한다.
        
        블랙말은 Pawn 부터 시작해서 마지막 King 까지 각자 말들의 위치에서 갈수있는 범위 안에 White라는 말이 있다면 pieceSearchPos라는 bool 변수를 true로 바꾸고 
        AIObject 스크립트에서 RandomMove함수를 발동시킨다.

        만약 자신이 갈수 있는 범위 안에 화이트 말이들이 없다면 bool 값은 false 이며, 자동적으로 일반적인 블랙말들의 이동이 있을것이다. 이것 또한 마찬가지로 false 일때 AIObject스크립트에서
        RandomMove 함수를 발동시킨다.

       
      
        */









        // MovePieceNumber 
        for (MovePieceNumber = 0; MovePieceNumber <= 15; MovePieceNumber++)
        {
            AIObject.piecesSearchPos = false;
            PieceNumber = ChessBoardManager.GetComponent<ChessBoard>().chess[MovePieceNumber];
            if (PieceNumber == null) continue;

            //폰 경로 안에 적을 탐색
            #region 폰 경로
            if (MovePieceNumber <= 7)
            {
                PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
                oop = PieceNumber.GetComponent<AIObject>().searchPos;


                if (PieceNumCurrentposition == 8 )
                {
                    if (oop.x >= 0 && oop.y >= 0)
                        if (ChessBoard.chessBoard[PieceNumCurrentposition - 7].Substring(0, 1) == "W")
                        {
                            AIObject.piecesSearchPos = true;
                            break;
                        }
                 }

                if (ChessBoard.chessBoard[PieceNumCurrentposition - 9].Length > 0 && oop.x > 0 && oop.y > 0)
                {                 

                    if (ChessBoard.chessBoard[PieceNumCurrentposition - 9].Substring(0, 1) == "W")
                    {
                        AIObject.piecesSearchPos = true;                     
                        break;

                    }
                }

               // int tmpp = PieceNumber.GetComponent<AIObject>().currentPosition;
                PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
                oop = PieceNumber.GetComponent<AIObject>().searchPos;
                if (ChessBoard.chessBoard[PieceNumCurrentposition - 7].Length > 0 && oop.x < 8 && oop.y >= 0)
                    if (ChessBoard.chessBoard[PieceNumCurrentposition - 7].Substring(0, 1) == "W")
                    {
                        AIObject.piecesSearchPos = true;
                        break;
                    }
            }
            #endregion
            //룩 경로 안에 적을 탐색
            #region 룩 경로
            if (MovePieceNumber > 7 && MovePieceNumber <= 9)
            {
                int PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
                oop = PieceNumber.GetComponent<AIObject>().searchPos;
                

                for (int direction = 0; direction < 4; direction++)
                {
                    PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                    PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                    oop = PieceNumber.GetComponent<AIObject>().searchPos;
                    while (true)
                    {
                        
                        if (direction == UP) //UP
                        {

                            directionNum = UP;
                            if (++oop.y >= 8) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                            {
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    print("rook ok");
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                        }

                        if (direction == RIGHT) //RIGHT
                        {
                            //PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                            //PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                            directionNum = RIGHT;

                            if (++oop.x >= 8) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                            {
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                   // AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                            
                        }

                        if (direction == DOWN) //DOWN
                        {
                            //PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                            //PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                            directionNum = DOWN;
                            if (--oop.y < 0) break;

                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
                            {
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    print("됫네?");
                                    break;
                                }
                                else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }

                        }


                        if (direction == LEFT) //LEFT
                        {
                            //PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                            //PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                            directionNum = LEFT;
                            if (--oop.x < 0) break;

                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
                            {
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;

                                    break;
                                }
                                else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (AIObject.piecesSearchPos)  break;
                }
            }
            #endregion
            //비숍 경로 안에 적을 탐색
            #region 비숍 우선순위
            if (MovePieceNumber > 9 && MovePieceNumber <= 11)
            {
                int PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
                oop = PieceNumber.GetComponent<AIObject>().searchPos;

                for (int direction = 0; direction < 4; direction++)
                {
                    PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                    PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                    oop = PieceNumber.GetComponent<AIObject>().searchPos;

                   
                    while (true)
                    {
                        if (direction == QUADRANT1) //제 1사분면
                        {
                            if (++oop.y >= 8 || ++oop.x >= 8) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                            {
                                
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                           
                        }
                        else if (direction == QUADRANT2) //제 2사분면
                        {
                            if (++oop.y >= 8 || --oop.x < 0) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
                            {
                                
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                   // AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                           
                        }
                        else if (direction == QUADRANT3) //제 3사분면
                        {
                            if (--oop.x < 0 || --oop.y < 0) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x >= 0 && oop.y >= 0)
                            {
                                
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                           
                        }
                        else if (direction == QUADRANT4) //제 4사분면
                        {
                            if (++oop.x >= 8 || --oop.y < 0) break;
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
                            {
                               
                                if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                                {
                                    AIObject.piecesSearchPos = true;
                                    break;
                                }
                                else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                                {
                                    //AIObject.piecesSearchPos = false;
                                    break;
                                }
                            }
                           
                        }
                    }
                    if (AIObject.piecesSearchPos) break;
                }

            }
            #endregion
            //나이트 경로 안에 적을 탐색
            #region 나이트
            if (MovePieceNumber > 11 && MovePieceNumber <=13)
            {
                PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
                oop = PieceNumber.GetComponent<AIObject>().searchPos;

                for (int direction = 0; direction < 8; direction++)
                {
                    PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
                    PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

                    oop = PieceNumber.GetComponent<AIObject>().searchPos;

                    if (direction == UP3) // y+2 x+1
                    {
                        if ((oop.y += 2) >= 8 || ++oop.x >= 8) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                              //  AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }
                       
                    }
                    else if (direction == UP2) //y+2 x-1
                    {
                        if ((oop.y += 2) >= 8 || --oop.x < 0) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }

                    }
                    else if (direction == UP1) // x-2 y+1
                    {
                        if ((oop.x -= 2) < 0 || ++oop.y >= 8) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }

                    }
                    else if (direction == UP4) //x+2 y+1
                    {
                        if ((oop.x += 2) >= 8 || ++oop.y >= 8) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }
                        

                    }
                    else if (direction == DOWN1) //x-2 y-1
                    {
                        if ((oop.x -= 2) < 0 || --oop.y < 0) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }

                    }
                    else if (direction == DOWN2) //y-2 x-1
                    {
                        if ((oop.y -= 2) < 0 || --oop.x < 0) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }

                    }
                    else if (direction == DOWN3) //y-2 x+1
                    {
                        if ((oop.y -= 2) < 0 || ++oop.x >= 8) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }

                    }
                    else if (direction == DOWN4) //y-1 x+2
                    {
                        if ((oop.x += 2) >= 8 || --oop.y < 0) continue;
                        if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
                        {
                            if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
                            {
                                AIObject.piecesSearchPos = true;
                                continue;
                            }
                            else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
                            {
                                //AIObject.piecesSearchPos = false;
                                continue;
                            }
                            else
                            {
                               // AIObject.piecesSearchPos = false;
                                continue;
                            }
                        }
                    }
                }

            }
            #endregion
            //퀸 경로 안에 적을 탐색
            #region 퀸 경로적 확인
            if(MovePieceNumber > 14 && MovePieceNumber <= 15) 
			{ 
				int PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
				PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
				oop = PieceNumber.GetComponent<AIObject>().searchPos;

				for (int direction = 0; direction < 8; direction++)
				{
					PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
					PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

					oop = PieceNumber.GetComponent<AIObject>().searchPos;


					while (true)
					{


						if (direction == UP) //UP
						{

							directionNum = UP;
							if (++oop.y >= 8) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
							{
								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									
									AIObject.piecesSearchPos = true;
									break;
								}
								else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}
						}

						if (direction == RIGHT) //RIGHT
						{
							//PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
							//PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

							directionNum = RIGHT;

							if (++oop.x >= 8) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
							{
								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;
									break;
								}
								else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									// AIObject.piecesSearchPos = false;
									break;
								}
							}

						}

						if (direction == DOWN) //DOWN
						{
							//PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
							//PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

							directionNum = DOWN;
							if (--oop.y < 0) break;

							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
							{
								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;

									break;
								}
								else if(ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}

						}


						if (direction == LEFT) //LEFT
						{
							//PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
							//PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);

							directionNum = LEFT;
							if (--oop.x < 0) break;

							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
							{
								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;

									break;
								}
								else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}
						}
												

						if (direction == DOWN1) //제 1사분면
						{
							if (++oop.y >= 8 || ++oop.x >= 8) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)
							{

								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;
									break;
								}
								else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}

						}
						if (direction == DOWN2) //제 2사분면
						{
							if (++oop.y >= 8 || --oop.x < 0) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
							{

								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;
									break;
								}
								else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									// AIObject.piecesSearchPos = false;
									break;
								}
							}

						}
						if (direction == DOWN3) //제 3사분면
						{
							if (--oop.x < 0 || --oop.y < 0) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x >= 0 && oop.y >= 0)
							{

								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;
									break;
								}
								else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}

						}
						if (direction == DOWN4) //제 4사분면
						{
							if (++oop.x >= 8 || --oop.y < 0) break;
							if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
							{

								if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "W")
								{
									AIObject.piecesSearchPos = true;
									break;
								}
								else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
								{
									//AIObject.piecesSearchPos = false;
									break;
								}
							}
						}
					}
					if (AIObject.piecesSearchPos) break;
				}       
        	}


        #endregion
            //킹 경로 안에 적을 탐색
			#region 킹 경로에 적을 확인.

			if(MovePieceNumber > 13 && MovePieceNumber <=14){

			int PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
			PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
			oop = PieceNumber.GetComponent<AIObject>().searchPos;

			for (int direction = 0; direction < 8; direction++)
			{
				
				PieceNumCurrentposition = PieceNumber.GetComponent<AIObject>().currentPosition;
				PieceNumber.GetComponent<AIObject>().searchPos = PieceNumber.GetComponent<AIObject>().calcXYfromPosition(PieceNumCurrentposition);
				oop = PieceNumber.GetComponent<AIObject>().searchPos;

				if (direction == UP) //UP
				{
						if (++oop.y >= 8) continue;
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y <= 8)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{
							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
							continue;
					}
					

				}
				else if (direction == RIGHT) //RIGHT
				{
						if (++oop.x >= 8) continue;
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x <= 8 && oop.y <=8)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{

							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					
				}
				else if (direction == DOWN) //DOWN
				{
						if (--oop.y < 0) continue;
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{
							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue; 
					}
					
				}
				else if (direction == LEFT) //LEFT
				{
						if (--oop.x < 0) continue;
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{

							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					
				}
				else if (direction == DOWN1) //y+1 x-1
				{
						if (--oop.x < 0 || ++oop.y >= 8) continue;

						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y < 8)

						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{

							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					
					
				}
				else if (direction == DOWN2) //x+1 y=1
				{
						if (++oop.x >= 8 || ++oop.y >= 8) continue;

						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{
							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					

				}
				else if (direction == DOWN3) //x+1 y-1
				{
						if (--oop.y < 0 || ++oop.x >= 8) continue;
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{

							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					
				}
				else if (direction == DOWN4) //x-1 y-1
				{
						if (--oop.y < 0 || --oop.x < 0) continue;    
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Length > 0 && oop.x < 8 && oop.y >= 0)
						if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0,1) == "W")
					{

							AIObject.piecesSearchPos = true;
							break;
					}
						else if (ChessBoard.chessBoard[PieceNumber.GetComponent<AIObject>().calcPositionByXY(oop)].Substring(0, 1) == "B")
					{
						continue;
					}
					
				}
			}
		}






            #endregion

            if (AIObject.piecesSearchPos)   break;          
            
        }

        if(AIObject.piecesSearchPos == true)
        {          
           
            PieceNumber.GetComponent<AIObject>().RandomMove();            
        }


        if (AIObject.piecesSearchPos == false)
        {
            while (true)
            {
                if (++whiletmp > 100) break;
                int nextMovePieceNum = Random.Range(0, 16);
                while (true)
                {
                    if (ChessBoardManager.GetComponent<ChessBoard>().chess[nextMovePieceNum] == null)
                    {
                        nextMovePieceNum = Random.Range(0, 16);
                    }
                    else
                        break;
                }
                nextMovePiece = ChessBoardManager.GetComponent<ChessBoard>().chess[nextMovePieceNum];
                STnextMovePiece = nextMovePiece;              
                print(STnextMovePiece);
                if (nextMovePiece.GetComponent<AIObject>().RandomMove()) break;

            }

        }
    }


    IEnumerator delayAnim()//이동시 딜레이를 주기위하여 
    {
        AnimStr();
        //force
        switch (WhiteN)
        {

            case "pawn":
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forcePawnW, transform.position, transform.rotation);
                }
              
                break;

            case "knight":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forceKnightW, transform.position, transform.rotation);
                }
               
                break;

            case "rook":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forceRookW, transform.position, transform.rotation);
                }
               
                break;

            case "bishop":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forceBishopW, transform.position, transform.rotation);
                }
                

                break;

            case "king":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forceKingW, transform.position, transform.rotation);
                }
              
                break;

            case "queen":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player1")
                {
                    Instantiate(forceQueenW, transform.position, transform.rotation);
                }
               
                break;

        }
        yield return new WaitForSeconds(0.5f);
        AnimEnd();
        if (WhiteN == "pawn")
        {
            yield return new WaitForSeconds(1);
        }
        else if(WhiteN == "bishop")
        {
            yield return new WaitForSeconds(3);
        }
        else if(WhiteN == "rook")
        {
            yield return new WaitForSeconds(3);
        }
        //yield return new WaitForSeconds(1);
       
        Go = true;

    }

  


}
