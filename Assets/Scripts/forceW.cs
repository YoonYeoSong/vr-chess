﻿using UnityEngine;
using System.Collections;

public class forceW : MonoBehaviour {

    public float speed = 1.0f;
    Transform tr;
	GameObject fragMent;
    //public Vector3 bulletPos = Vector3.zero;



    // Use this for initialization
    void Start()
    {
		fragMent = Resources.Load ("FragMent") as GameObject;
        tr = GetComponent<Transform>();
        tr.GetComponent<Rigidbody>().AddForce(tr.forward * speed);
      //  bulletPos = tr.position;
       // Destroy(gameObject);//, 3.0f);
    }
    void OnTriggerEnter(Collider coll) {
        if (coll.GetComponentInParent<Transform>().tag == "Player2") {
			Instantiate (fragMent, coll.transform.position , transform.rotation);
			Debug.Log (coll.transform.position);
            Destroy(coll.gameObject);
            Destroy(gameObject);
        }
    }
}
