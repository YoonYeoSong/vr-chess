﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/*
    AIObject 스크립트는 블랙 말들이 자동적으로 움직이게 해주는 스크립트이다.
    처음에 string 변수로 objName에 각각 말마다 폰이면 "pawn" , 나이트면 "knight" 라고 스트링으로 이름을 정해준다.
    변수에 currentPosition 각각 현재 기물의 위치값을 알려준다 체스 판은 0~63개로 이루어진 배열의 판이라고 보면된다. 그 각각의 위치값을 처음에 말들에 적용시킨다.
    
    화이트 chessMove스크립트에서 아까 말한 picesSearchPos 가 true 일때 RandomMove함수가 발동되며, piceSearchPos가 true가 되기전에 블랙 말중에서 자신이 갈수 있는 위치에
    화이트 말이 있다는걸 감지하고 true로 변경되고 바로 AI스크립트로 넘어온것이다. 잡아먹기위해서... 그 걸 감지한 블랙 말이 RandomMove() 함수 에서 다시한번 탐색을 시작한다,
    예를 들어 설명해주겠다.
    
    이 밑의 코드는 picesSearchPos 룩이 자신이 갈수있는 위치에 확인되어 true 일시 발동되는 스크립트이다. 피스서치포스가 true가되고 다시 한번 탐색한다 for문으로 0~4까지의 값을 주었고
    0 부터 UP RIGHT DOWN LEFT 총 룩은 4방향 위 아래 오른쪽 왼쪽을 갈수있다. 예시로 위 방향을 예로 들어주겠다.
    0일때 위 방향으로 searchPos.y값 즉 좌표이다 체스이기때문에 8x8의 좌표 배치도이다. y는 8까지 계속 증가하며 한칸한칸 탐색할것이다. 언제까지? 
    ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W" W라는 스트링값이 나올때까지 아무도 없는공간은 "" 널을 넣어줬고 자신의 동료인 B블랙의 위치가 있을경우 브레이크를
    걸어 주었다. W일때만 잡아먹게된다.

    y가 만약 0일때 8까지 한칸 한칸 증가하면서 탐색을 시작한다. 0일때 ""값이면 availablePosition.Add(calcPositonByXY(searchPos)); 함수로 그 탐색한 위치값을 갈수 있는 위치로 추가해준다.
    그래서  public List<int> availablePosition; 리스트를 사용하였다. 한칸 한칸 탐색하다가 6가 6일때 "W"가 있다면 바로 잡아먹게 되는것이다. piceSearchPos가 true일경우와 false일경우 두가지
    경우를 두었기 때문에 트루일때는 잡아먹게하는거고 만약 false일경우는 "W"의 값이 "" 널값으로 바뀌어서 시작된다. 공백일 경우에 그 위치로 가라고하는 것이다.

    이 코드의 경우 탐색만 진행하면서 그 리스트에 추가해주는것이고 이 탐색이 끝이나고 나면 Move()라는 함수가 시작된다. 무브함수에는 sPos dPos 시작 점과 도착점 처음에 시작점은 currentPosition값
    이 들어가 있고 destination값에는 각각 판의 배열위치 값이 들어가있다. 유니티 안에 Vector3.Lerp라는 함수를 이용하여 처음 있는 위치에서 도착점있는 위치까지 전진하게 된다. 그리하여 상대를 잡아먹거나
    이동할때 발동된다.

    이렇게 블랙의 이동이 끝이나면 다시 화이트 말을 플레이어가 조작하여 움직이고 이걸 반복적으로 수행하게된다.


    if (piecesSearchPos == true)
                {
                  
                    for (int direction = 0; direction < 4; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);

                        while (true)
                        {

                            if (direction == UP)
                            {

                                if (++searchPos.y >= 8) break;

                                    if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y <= 8)
                                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.Rotate(0, -90f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                            break;
                                        }
                                        else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                        {
                                            break;
                                        }
                            }


*/



public class AIObject : MonoBehaviour {

    #region 상수들
    // rook 상수
    const int UP = 0;
    const int RIGHT = 1;
    const int DOWN = 2;
    const int LEFT = 3;

    
    //bishop 상수
    const int QUADRANT1 = 0;
    const int QUADRANT2 = 1;
    const int QUADRANT3 = 2;
    const int QUADRANT4 = 3;

    //knight 상수
    const int UP1 = 0;
    const int UP2 = 1;
    const int UP3 = 2;
    const int UP4 = 3;
    const int DOWN1 = 4;
    const int DOWN2 = 5;
    const int DOWN3 = 6;
    const int DOWN4 = 7;
    
    

   
    #endregion


    public string objName;  //이 기물의 이름(개발자 직접 인스펙터에서 지정)
    
    GameObject panArray;    //전체 보드의 게임 오브젝트
    public int currentPosition; //현재 기물의 위치 (초기값만 지정, 나머지는 자동계산)
    public Vector3 Chessposition;      //현재 기물의 실제 위치(Vector3, 자동계산)
    float count = 0;
    public Vector2 searchPos;
    public static bool piecesSearchPos;
    public static int CurrentPositonStatic;
    bool Rook_=true; // 룩경로의 while문 bool 변수
    bool Knight_ = true; // 나이트 경로의 while문 bool 변수
    bool Queen_ = true;

    public bool IsBMove = false; // 블랙 말이 움직일때 참인지 거짓인지 구별.

    public static GameObject[] ChessPieces;

    GameObject promotionBqueen;
    GameObject promotionBbishop;
    GameObject promotionBknight;
    GameObject promotionBrook;

    public static int isChessN = 0;
    public List<int> availablePosition;

    GameObject forcePawnB, forceRookB, forceBishopB, forceKnightB, forceKingB, forceQueenB;

    ///////////////////////애니메이션

    public Animator AIanimator;
    /////////////////////////


    void AIanimStr()
    {
        AIanimator.SetBool("attack", true);
        Debug.Log(AIanimator.GetBool("attack"));
        if (AIanimator != null)
        {
            AIanimator.SetBool("attack", true);
        }
    }

    /// <summary>
    /// 애니메이션 끝
    /// </summary>
    void AIanimEnd()
    {
        if (AIanimator != null)
        {
            AIanimator.SetBool("attack", false);
        }
    }

    void Awake()
    {      
        forcePawnB = Resources.Load("forcePawnB") as GameObject;       
        forceRookB = Resources.Load("forceRookB") as GameObject;       
        forceBishopB = Resources.Load("forceBishopB") as GameObject;
		forceKingB = Resources.Load("forceKingB") as GameObject;
		forceKnightB = Resources.Load("forceKnightB") as GameObject;
		forceQueenB = Resources.Load("forceQueenB") as GameObject;
    }

	// Use this for initialization
    //폰 0~7 룩 8 ~9 비숍 10~11 나이트 12~13 킹 14 퀸 15
	void Start () {
        
        //position = GameObject.Find("blue" + 0).transform.position;
        panArray = GameObject.FindGameObjectWithTag("panArray");
        
        Chessposition = getVector3Position(currentPosition);

        promotionBbishop = GameObject.FindGameObjectWithTag("promoteBishop");
        promotionBqueen = GameObject.FindGameObjectWithTag("promoteQueen");
        promotionBknight = GameObject.FindGameObjectWithTag("promoteKnight");
        promotionBrook = GameObject.FindGameObjectWithTag("promoteRook");

    }
	
	// Update is called once per frame
	void Update () {
        
	}

    IEnumerator Move(int destinationPos)
    {
        
        //실제 현재 위치와 이동될 위치의 Vector3 값 구하기
        Vector3 sPos = Chessposition;
        sPos.y += 0.6f;
        Vector3 dPos = getVector3Position(destinationPos);

        //static chessboard 및 currentPosition 갱신 코드
        ChessBoard.chessBoard[currentPosition] = "";
        ChessBoard.chessBoard[destinationPos] = "B" + objName;
        currentPosition = destinationPos;

        

        if (piecesSearchPos == true)
        {
            StartCoroutine(AIdelayAnim());
        }
        yield return new WaitForSeconds(3);


        if (objName == "Bknight"){
			GetComponent<BoxCollider> ().enabled = false;
		}

       
       


        //이동 애니메이션
        for (count = 0f; count <= 1.0f; count += 0.05f)
        {           
            dPos.y = 0;
            transform.position = Vector3.Lerp(sPos, dPos, count);
            yield return new WaitForSeconds(0.001f);     

        }        
        transform.position = dPos;
        transform.rotation = Quaternion.Euler(0, 180f, 0);
        yield return new WaitForSeconds(1);

		if(objName == "Bknight"){

			GetComponent<BoxCollider> ().enabled = true;

		}
        Rook_ = true;
        Queen_ = true;


    }

    public Vector3 getVector3Position(int position)
    {
        Transform[] trs = panArray.transform.GetComponentsInChildren<Transform>();
        foreach (Transform t in trs)
        {
            if (t.name == ("blue" + position))
            {
                return t.position;
            }
        }
        return Vector3.zero;
    }

    /// <summary>
    /// 현재 기물을 랜덤으로 움직이게 하는 함수
    /// </summary>
    /// <returns></returns>
    public bool RandomMove()
    {
        //현재 기물의 실제 Vector3 위치를 계산
        Chessposition = getVector3Position(currentPosition);

        //현재 기물의 이동가능한 칸을 계산
        calcAvailablePosition();

        //현재 기물이 이동가능한 칸이 없으면 false 반환
        if (availablePosition.Count == 0) return false;

        //다음 이동 장소를 랜덤으로 선택
        int nextPosition = Random.Range(0, availablePosition.Count);

        if (chessMove.STnextMovePiece == null)
        {
            GetComponent<chessMove>().delay();
        }

        //chessMove.STnextMovePiece.GetComponent<BoxCollider>().enabled = false;
        //이동
        StartCoroutine(Move(availablePosition[nextPosition]));
        //chessMove.STnextMovePiece.GetComponent<BoxCollider>().enabled = true;
        if (objName == "pawn" && calcXYfromPosition(availablePosition[nextPosition]).y == 0)
        {
            // 4개중 하나의 이름으로 바뀌고 
            string[] promotePieces = { "queen", "bishop", "rook", "knight" };
            string promoteResult = promotePieces[Random.Range(0, promotePieces.Length)];
            objName = promoteResult;

            //그래픽 덮어씌우기

            Destroy(transform.Find("Pawn").gameObject);
            GameObject newPiece = null;

            //폰이 끝에 갔을경우 4개중 하나로 오브젝트변경
            switch (objName)
            {
                #region 폰 나이트 퀸 비숍 위치와 모델변경 
                case "queen":
                    newPiece = Instantiate(promotionBqueen);
                    newPiece.transform.parent = transform;
                    newPiece.transform.localPosition = new Vector3(2.543f, -0.524f, 0.32f);
                    break;
                case "bishop":
                    newPiece = Instantiate(promotionBbishop);
                    newPiece.transform.parent = transform;

                    newPiece.transform.localPosition = new Vector3(0.159f, 0.11f, 0.092f);
                    break;
                case "rook":
                    newPiece = Instantiate(promotionBrook);
                    newPiece.transform.parent = transform;
                    newPiece.transform.localPosition = new Vector3(0f, -0.5f, 0f);
                    break;
                case "knight":
                    newPiece = Instantiate(promotionBknight);
                    newPiece.transform.parent = transform;
                    newPiece.transform.localPosition = new Vector3(0f, -0.53f, 0f);
                    break;
                    #endregion
            }
        }

        //이동 성공 반환
        return true;
    }

    /// <summary>
    /// 현재 기물이 이동할 수 있는 다음 위치들을 계산하여 availablePosition 배열에 넣어준다.
    /// </summary>
    public void calcAvailablePosition()
    {
        availablePosition.Clear();

        //searchPos = calcXYfromPosition(currentPosition);

       

        //각 말들의 이동경로 케이스
        switch (objName)
        {
            case "pawn":
                #region 폰 경로 계산 케이스
                searchPos = calcXYfromPosition(currentPosition);


                if (calcXYfromPosition(currentPosition).y == 6)//폰이 출발지점(y좌표 6)에 있을 때에는 두칸 전진이 가능하다.
                {
                    if (piecesSearchPos == true)
                    {

                        if (currentPosition == 8)
                        {
                            if( searchPos.x >= 0 && searchPos.y >= 0)
                                if (ChessBoard.chessBoard[currentPosition - 7].Substring(0, 1) == "W")
                                {
                                    searchPos.x++;
                                    searchPos.y--;
                                    transform.Rotate(0, -45, 0);        
                                }
                        }
                        searchPos = calcXYfromPosition(currentPosition);


                        if (ChessBoard.chessBoard[currentPosition - 9].Length > 0 && searchPos.x > 0 && searchPos.y > 0)

                            if (ChessBoard.chessBoard[currentPosition - 9].Substring(0, 1) == "W")
                            {
                                searchPos.y--;
                                searchPos.x--;
                                transform.Rotate(0, 45, 0);
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                        searchPos = calcXYfromPosition(currentPosition);


                        if (ChessBoard.chessBoard[currentPosition - 7].Length > 0 && searchPos.x < 7 && searchPos.y > 0)
                            if (ChessBoard.chessBoard[currentPosition - 7].Substring(0, 1) == "W")
                            {
                                searchPos.y--;
                                searchPos.x++;
                                transform.Rotate(0, -45, 0);
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                        searchPos = calcXYfromPosition(currentPosition);

                    }
                    if (piecesSearchPos == false)
                    {
                        --searchPos.y;
                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                        {
                            availablePosition.Add(calcPositionByXY(searchPos));
                            --searchPos.y;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                        }
                    }
                }


                else
                {
                    if (piecesSearchPos == true)
                    {
                        if (ChessBoard.chessBoard[currentPosition - 9].Length > 0 && searchPos.x > 0 && searchPos.y > 0)
                            if (ChessBoard.chessBoard[currentPosition - 9].Substring(0, 1) == "W")
                            {
                                searchPos.y--;
                                searchPos.x--;
                                transform.Rotate(0, 45, 0);
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                        searchPos = calcXYfromPosition(currentPosition);

                        //searchPos = calcXYfromPosition(currentPosition);

                        if (ChessBoard.chessBoard[currentPosition - 7].Length > 0 && searchPos.x < 7 && searchPos.y > 0)
                            if (ChessBoard.chessBoard[currentPosition - 7].Substring(0, 1) == "W")
                            {
                                searchPos.y--;
                                searchPos.x++;
                                transform.Rotate(0, -45, 0);
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }

                        searchPos = calcXYfromPosition(currentPosition);
                    }
                    if(piecesSearchPos == false)
                    {
                        --searchPos.y;
                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                        {
                            availablePosition.Add(calcPositionByXY(searchPos));
                        }
                    }                
                }
            
                break;
            #endregion
            case "rook":
                #region 룩 경로 계산 케이스
                if (piecesSearchPos == true)
                {
                  
                    for (int direction = 0; direction < 4; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);

                        while (Rook_)
                        {

                            if (direction == UP)
                            {
                                if (++searchPos.y >= 8) break;

                                    if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y <= 8)
                                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        //transform.Rotate(0, 180f, 0);
                                        Debug.Log("UP");
                                        transform.rotation = Quaternion.Euler(0, 360f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Rook_ = false;
                                            break;
                                        }
                                        else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                        {
                                            break;
                                        }
                            }

                           else if (direction == RIGHT)
                            {
                                if (++searchPos.x >= 8) break;

                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x <= 8 && searchPos.y <=8)
                                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        Debug.Log("RIGHT");
                                        // transform.Rotate(0, -90f, 0);
                                        // transform.rotation = Quaternion.EulerAngles(0f, 90f, 0f);
                                        transform.rotation = Quaternion.Euler(0, 90f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Rook_ = false;
                                        break;

                                    }
                                    else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                    {
                                        break;
                                    }
                            }


                           else if (direction == DOWN) //DOWN
                            {
                                if (--searchPos.y < 0) break;

                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
                                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 180f, 0);
                                        Debug.Log("DOWN");
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Rook_ = false;
                                        break;

                                    }
                                    else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                    {
                                        break;
                                    }

                            }
                         else if (direction == LEFT) //LEFT
                            {
                                if (--searchPos.x < 0) break;

                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
                                        if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        Debug.Log("LEFT");
                                        // transform.Rotate(0, 90f, 0);
                                        transform.rotation = Quaternion.Euler(0, 270f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Rook_ = false;
                                        break;

                                    }
                                    else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                    {
                                        break;
                                    }
                            }
                        }
                    }

                    
                }
                

                if (piecesSearchPos == false)
                {
                    for (int direction = 0; direction < 4; direction++)
                    {
                    searchPos = calcXYfromPosition(currentPosition);
                    
                        while (true)
                        {
                            if (direction == UP) //UP
                            {
                                if (++searchPos.y >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == RIGHT) //RIGHT
                            {
                                if (++searchPos.x >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN) //DOWN
                            {
                                if (--searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == LEFT) //LEFT
                            {
                                if (--searchPos.x < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                        }
                    }
                }

                break;
            #endregion
            case "bishop":
                #region  비숍 경로 계산 케이스
                if(piecesSearchPos == true)
                {
                    
                    for (int direction = 0; direction < 4; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);
                        while (true)
                        {
                            if (direction == QUADRANT1) //제 1사분면
                            {
                                if (++searchPos.y >= 8 || ++searchPos.x >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                    
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
                                {
                                        transform.rotation = Quaternion.Euler(0, 45f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        break;
                                    }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                       // transform.Rotate(0, 45f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        break;
                                }
                            }
                            else if (direction == QUADRANT2) //제 2사분면
                            {
                                if (++searchPos.y >= 8 || --searchPos.x < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)
                                   
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 315f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        break;
                                    }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                        //transform.Rotate(0, -45f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == QUADRANT3) //제 3사분면
                            {
                                if (--searchPos.x < 0 || --searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x >= 0 && searchPos.y >= 0)
                                    
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                        transform.rotation = Quaternion.Euler(0, 225f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        break;
                                    }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                    {
                                        //transform.Rotate(0, 225f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == QUADRANT4) //제 4사분면
                            {
                                if (++searchPos.x >= 8 || --searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)
                                   
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 135f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        break;
                                    }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                    {
                                        //transform.Rotate(0, 135f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                        }
                    }
                }



                if (piecesSearchPos == false)
                {



                    for (int direction = 0; direction < 4; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);
                        while (true)
                        {
                            if (direction == QUADRANT1) //제 1사분면
                            {
                                if (++searchPos.y >= 6 || ++searchPos.x >= 6) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == QUADRANT2) //제 2사분면
                            {
                                if (++searchPos.y >= 6 || --searchPos.x < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == QUADRANT3) //제 3사분면
                            {
                                if (--searchPos.x < 0 || --searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == QUADRANT4) //제 4사분면
                            {
                                if (++searchPos.x >= 6 || --searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }









                        }
                    }
                }
                break;
            #endregion
            case "knight":
                #region 나이트 경로 계산 케이스

                if(piecesSearchPos == true)
                {
                   
                    for (int direction = 0; direction < 8; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);

                        if (direction == UP3) // y+2 x+1
                        {
                            if ((searchPos.y += 2) >= 8 || ++searchPos.x >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {

                                    transform.rotation = Quaternion.Euler(0,30f,0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                   
                                    continue;
                                }
                                else
                                {
                                    
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == UP2) //y+2 x-1
                        {
                            if ((searchPos.y += 2) >= 8 || --searchPos.x < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 330f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    
                                    continue;
                                }
                                else
                                {
                                   
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == UP1) // x-2 y+1
                        {
                            if ((searchPos.x -= 2) < 0 || ++searchPos.y >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 295f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                   
                                    continue;
                                }
                                else
                                {                                 
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == UP4) //x+2 y+1
                        {
                            if ((searchPos.x += 2) >= 8 || ++searchPos.y >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 62f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                   
                                    continue;
                                }
                                else
                                {                                 
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == DOWN1) //x-2 y-1
                        {
                            if ((searchPos.x -= 2) < 0 || --searchPos.y < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 250f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                   
                                    continue;
                                }
                                else
                                {
                                    //transform.Rotate(0, -300.4f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == DOWN2) //y-2 x-1
                        {
                            if ((searchPos.y -= 2) < 0 || --searchPos.x < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 210f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    
                                    continue;
                                }
                                else
                                {
                                    //transform.Rotate(0, 26.6f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == DOWN3) //y-2 x+1
                        {
                            if ((searchPos.y -= 2) < 0 || ++searchPos.x >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 155f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {                               
                                    continue;
                                }
                                else
                                {                                 
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                        else if (direction == DOWN4) //y-1 x+2
                        {
                            if ((searchPos.x += 2) >= 8 || --searchPos.y < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                {
                                    transform.rotation = Quaternion.Euler(0, 125f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    
                                    continue;
                                }
                                else
                                {                                
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    continue;
                                }
                        }
                    }

                }

                if (piecesSearchPos == false)
                {

                    for (int direction = 0; direction < 8; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);

                        if (direction == UP3) // y+2 x+1
                        {
                            if ((searchPos.y += 2) >= 8 || ++searchPos.x >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == UP2) //y+2 x-1
                        {
                            if ((searchPos.y += 2) >= 8 || --searchPos.x < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == UP1) // x-2 y+1
                        {
                            if ((searchPos.x -= 2) < 0 || ++searchPos.y >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == UP4) //x+2 y+1
                        {
                            if ((searchPos.x += 2) >= 8 || ++searchPos.y >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == DOWN1) //x-2 y-1
                        {
                            if ((searchPos.x -= 2) < 0 || --searchPos.y < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == DOWN2) //y-2 x-1
                        {
                            if ((searchPos.y -= 2) < 0 || --searchPos.x < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == DOWN3) //y-2 x+1
                        {
                            if ((searchPos.y -= 2) < 0 || ++searchPos.x >= 8) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                        else if (direction == DOWN4) //y-1 x+2
                        {
                            if ((searchPos.x += 2) >= 8 || --searchPos.y < 0) continue;
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                                continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                                continue;
                            }
                        }
                    }
                }
                
                break;
            #endregion
            case "king":
                #region  킹 경로 계산 케이스
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////킹
			if(piecesSearchPos == true)
			{

				for (int direction = 0; direction < 8; direction++)
				{
					searchPos = calcXYfromPosition(currentPosition);

					if (direction == UP) //UP
					{
						if (++searchPos.y >= 8) continue;
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y <= 8)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 360f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}

					}
					else if (direction == RIGHT) //RIGHT
					{
						if (++searchPos.x >= 8) continue;
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x <= 8 && searchPos.y <=8)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 90f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));                                    
                                    break;

						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
					else if (direction == DOWN) //DOWN
					{
						if (--searchPos.y < 0) continue;
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
							availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue; 
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
					else if (direction == LEFT) //LEFT
					{
						if (--searchPos.x < 0) continue;
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 270f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
					else if (direction == DOWN1) //y+1 x-1
					{
						if (--searchPos.x < 0 || ++searchPos.y >= 8) continue;

						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)
							
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 315f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
					else if (direction == DOWN2) //x+1 y=1
					{
						if (++searchPos.x >= 8 || ++searchPos.y >= 8) continue;

						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 45f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}

					}
					else if (direction == DOWN3) //x+1 y-1
					{
						if (--searchPos.y < 0 || ++searchPos.x >= 8) continue;
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 135f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
					else if (direction == DOWN4) //x-1 y-1
					{
						if (--searchPos.y < 0 || --searchPos.x < 0) continue;    
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)
						if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
						{
                                    transform.rotation = Quaternion.Euler(0, 225f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
						}
						else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
						{
							continue;
						}
						else
						{
							availablePosition.Add(calcPositionByXY(searchPos));
							continue;
						}
					}
				}
			}
				

			if(piecesSearchPos == false){
                for (int direction = 0; direction < 8; direction++)
                {
                    searchPos = calcXYfromPosition(currentPosition);

                        if (direction == UP) //UP
                        {
                            if (++searchPos.y >= 8) continue;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    continue;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                            		continue;
                        		}

                        }

                        else if (direction == RIGHT) //RIGHT
                        {
                            if (++searchPos.x >= 8) continue;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {

                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                            		continue;
                       			 }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
	                            	continue;
	                        	}
                        }

                        else if (direction == DOWN) //DOWN
                        {
                            if (--searchPos.y < 0) continue;

                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                            continue; 
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                            continue;
                                }
                        }

                        else if (direction == LEFT) //LEFT
                        {
                            if (--searchPos.x < 0) continue;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {

                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                           			continue;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                            		continue;
                                }
                        }

                        else if (direction == DOWN1) //y+1 x-1
                        {
                            if (--searchPos.x < 0 || ++searchPos.y >= 8) continue;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {

                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                            		continue;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                            		continue;
                                }
                        }

                        else if (direction == DOWN2) //x+1 y=1
                        {
                            if (++searchPos.x >= 8 || ++searchPos.y >= 8) continue;


                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                           			 continue;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                            		continue;
                                }

                        }

                        else if (direction == DOWN3) //x+1 y-1
                        {
                            if (--searchPos.y < 0 || ++searchPos.x >= 8) continue;

                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {

                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                        			continue;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                        			continue;
                                }
                        }

                        else if (direction == DOWN4) //x-1 y-1
                        {
                            if (--searchPos.y < 0 || --searchPos.x < 0) continue;                      
                            if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                            {

                                availablePosition.Add(calcPositionByXY(searchPos));
                            }
                            else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                            {
                        		continue;
                            }
                            else
                            {
                                availablePosition.Add(calcPositionByXY(searchPos));
                            	continue;
                            }
                        }
                    }
			}



        
                break;
                #endregion
            case "queen":
			//////////////////////////////////////////////////////////////////////////////////////////////////////퀸
                #region 퀸 경로 계산 케이스
                if(piecesSearchPos == true)
                {
                    
					for (int direction = 0; direction < 8; direction++)
					{
					searchPos = calcXYfromPosition(currentPosition);

					while (Queen_)
					{

						if (direction == UP)
						{

							if (++searchPos.y >= 8) break;

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y <= 8)
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
							{
                                        transform.rotation = Quaternion.Euler(0, 360f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                break;
							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
						}

					else	if (direction == RIGHT)
						{
							if (++searchPos.x >= 8) break;

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x <= 8 && searchPos.y <=8)
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 90f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;

							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
						}


                            else if (direction == DOWN) //DOWN
						{
							if (--searchPos.y < 0) break;

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
							{
                                        transform.rotation = Quaternion.Euler(0, 180f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;

							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}

						}
                            else if (direction == LEFT) //LEFT
						{
							if (--searchPos.x < 0) break;

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8)
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
							{
                                        transform.rotation = Quaternion.Euler(0, 270f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;

							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
						}

                            else if (direction == DOWN1) //제 1사분면
						{
							if (++searchPos.y >= 8 || ++searchPos.x >= 8) break;
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y < 8)

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0,1) == "W")
							{
                                        transform.rotation = Quaternion.Euler(0, 45f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;
							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
							else
							{
								availablePosition.Add(calcPositionByXY(searchPos));
								break;
							}
						}
                            else if (direction == DOWN2) //제 2사분면
						{
							if (++searchPos.y >= 8 || --searchPos.x < 0) break;
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 315f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;
							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
							else
							{
								availablePosition.Add(calcPositionByXY(searchPos));
								break;
							}
						}
                            else if (direction == DOWN3) //제 3사분면
						{
							if (--searchPos.x < 0 || --searchPos.y < 0) break;
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x >= 0 && searchPos.y >= 0)

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                            {
                                    transform.rotation = Quaternion.Euler(0, 225f, 0);
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;
							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
							else
							{
								availablePosition.Add(calcPositionByXY(searchPos));
								break;
							}
						}
                            else if (direction == DOWN4) //제 4사분면
						{
							if (++searchPos.x >= 8 || --searchPos.y < 0) break;
							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Length > 0 && searchPos.x < 8 && searchPos.y >= 0)

							if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "W")
                                    {
                                        transform.rotation = Quaternion.Euler(0, 135f, 0);
                                        availablePosition.Add(calcPositionByXY(searchPos));
                                        Queen_ = false;
                                        break;
							}
							else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
							{
								break;
							}
							else
							{
								availablePosition.Add(calcPositionByXY(searchPos));
								break;
							}
						}
					}
				}				         
         }



                if (piecesSearchPos == false)
                {



                    for (int direction = 0; direction < 8; direction++)
                    {
                        searchPos = calcXYfromPosition(currentPosition);
                        while (true)
                        {
                            if (direction == UP) //UP
                            {
                                if (++searchPos.y >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == RIGHT) //RIGHT
                            {
                                if (++searchPos.x >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN) //DOWN
                            {
                                if (--searchPos.y < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == LEFT) //LEFT
                            {
                                if (--searchPos.x < 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN1) //x-1 y+1
                            {
                                if (--searchPos.x < 0 || ++searchPos.y >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN2) // x+1 y+1
                            {
                                if (++searchPos.x >= 8 || ++searchPos.y >= 8) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN3) //x+1 y-1
                            {
                                if (++searchPos.x >= 8 || --searchPos.y > 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                            else if (direction == DOWN4) //x-1 y-1
                            {
                                if (--searchPos.x > 0 || --searchPos.y > 0) break;
                                if (ChessBoard.chessBoard[calcPositionByXY(searchPos)] == "")
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                }
                                else if (ChessBoard.chessBoard[calcPositionByXY(searchPos)].Substring(0, 1) == "B")
                                {
                                    break;
                                }
                                else
                                {
                                    availablePosition.Add(calcPositionByXY(searchPos));
                                    break;
                                }
                            }
                        }
                    }
                }

                break;
                #endregion
               
        }
    }

    /// <summary>
    /// X, Y 좌표로 체스판의 배열 번호를 알아온다.
    /// </summary>
    /// <param name="x">알아올 번호의 X좌표</param>
    /// <param name="y">알아올 번호의 Y좌표</param>
    /// <returns>좌표에 대한 체스판의 배열 번호</returns>
    public int calcPositionByXY(int x, int y)
    {
        return y * 8 + x;
    }
    public int calcPositionByXY(Vector2 point)
    {
        return calcPositionByXY((int)point.x, (int)point.y);
    }

    /// <summary>
    /// 기물의 체스판 배열 위치로부터 X, Y 좌표값을 얻어온다.
    /// </summary>
    /// <param name="Position">기물이 있는 배열 번호</param>
    /// <returns>X, Y좌표 (Vector2)</returns>
    public Vector2 calcXYfromPosition(int Position) 
    {
        Vector2 result;
        result.x = Position % 8;
        result.y = Position / 8;
        return result;
    }


    IEnumerator AIdelayAnim()//이동시 딜레이를 주기위하여 
    {
        AIanimStr();
        //force
        switch (objName)
        {

            case "pawn":
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forcePawnB, transform.position, transform.rotation);
                }
               
                break;

            case "knight":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forceKnightB, transform.position, transform.rotation);
                }

                break;

            case "rook":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forceRookB, transform.position, transform.rotation);
                }

                break;

            case "bishop":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forceBishopB, transform.position, transform.rotation);
                }


                break;

            case "king":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forceKingB, transform.position, transform.rotation);
                }

                break;

            case "queen":
                yield return new WaitForSeconds(1);
                if (transform.parent.tag == "Player2")
                {
                    Instantiate(forceQueenB, transform.position, transform.rotation);
                }

                break;

        }
        yield return new WaitForSeconds(0.5f);
        AIanimEnd();

        if (objName == "pawn")
        {
            yield return new WaitForSeconds(1);
        }
        else if (objName == "bishop")
        {
            yield return new WaitForSeconds(3);
        }
        else if (objName == "rook")
        {
            yield return new WaitForSeconds(3);
        }


        
        

    }

}
