﻿using UnityEngine;
using System.Collections;

public class BlackPawn : MonoBehaviour {

    public static int pawncount;
    public static bool PawnFirst;
    public float count = 0;
    public static int Pawn1random;
    public int PMoverandom = 0;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

  


    IEnumerator MovePawn()
    {
        pawncount = Random.Range(0, 2);
        PMoverandom = chessMove.Pawn1random;

        Vector3 sPos = TotalChess.P2Pawn[PMoverandom].transform.position;

        if (PawnFirst == true)
        {

            if (chessMove.Pawn1random == 0)

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn0 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn0 - 16;

            if (chessMove.Pawn1random == 1)
           
                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn1 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn1 - 16;
           

             if(chessMove.Pawn1random == 2)
            

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn2 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn2 - 16;
            

             if (chessMove.Pawn1random == 3)
            

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn3 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn3 - 16;
            

            if (chessMove.Pawn1random == 4)
            

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn4 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn4 - 16;
            

            if (chessMove.Pawn1random == 5)
            

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn5 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn5 - 16;
            

            if (chessMove.Pawn1random == 6)
            
                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn6 - 8;
                else
                    TotalChess.PawnNumber = StartGm.Pawn6 - 16;
            

            if (chessMove.Pawn1random == 7)
            

                if (pawncount == 0)
                    TotalChess.PawnNumber = StartGm.Pawn7 - 8;
            
                else
                    TotalChess.PawnNumber = StartGm.Pawn7 - 16;
            

        }

        Vector3 dPos = TotalChess.Board[TotalChess.PawnNumber].transform.position;
        for (count = 0f; count < 1; count += 0.05f)
        {
            dPos.y = 0;
            transform.position = Vector3.Lerp(sPos, dPos, count);
            yield return new WaitForSeconds(0.001f);
        }

    }
    public void RealMovePawn()
    {
        StartCoroutine(MovePawn());
    }


}
