﻿using UnityEngine;
using System.Collections;

public class ChessBoard : MonoBehaviour {

    public static string[] chessBoard;
    public GameObject[] chess;
  	
	public GameObject[] Wchess;

    void Awake()
    {
       
        /* 1번 - 처음 시작 지점 Awake() 함수는 게임이 시작하는 동시에 활성화된다. 체스 보드 배열에 각각의 체스말의 위치를 스트링값으로 알려준다. */
        
        AIObject.ChessPieces = chess;


        chessBoard = new string[64];
        for (int i = 0; i < 63; i++)
            chessBoard[i] = "";

        //피스 위치 기록
        //White
        chessBoard[0] ="Wrook";
        chessBoard[1] ="Wknight";
        chessBoard[2] ="Wbishop";
        chessBoard[3] ="Wking";
        chessBoard[4] ="Wqueen";
        chessBoard[5] ="Wbishop";
        chessBoard[6] ="Wknight";
        chessBoard[7] ="Wrook";
        chessBoard[8] ="Wpawn";
        chessBoard[9] ="Wpawn";
        chessBoard[10] ="Wpawn";
        chessBoard[11] ="Wpawn";
        chessBoard[12] ="Wpawn";
        chessBoard[13] ="Wpawn";
        chessBoard[14] ="Wpawn";
        chessBoard[15] ="Wpawn";

        //Black

        chessBoard[48] = "Bpawn";
        chessBoard[49] = "Bpawn";
        chessBoard[50] = "Bpawn";
        chessBoard[51] = "Bpawn";
        chessBoard[52] = "Bpawn";
        chessBoard[53] = "Bpawn";
        chessBoard[54] = "Bpawn";
        chessBoard[55] = "Bpawn";
        chessBoard[56] = "Brook";
        chessBoard[57] = "Bknight";
        chessBoard[58] = "Bbishop";
        chessBoard[59] = "Bqueen";
        chessBoard[60] = "Bking";
        chessBoard[61] = "Bbishop";
        chessBoard[62] = "Bknight";
        chessBoard[63] = "Brook";
        






    }
    void OnMouseDown() {
        for(int i =0; i < 16; i++)
        {
            print(chess[i]);
        }
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
