﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
    public Collider[] colliders;
    // Use this for initialization
    void Awake()
    {
        colliders = gameObject.GetComponentsInChildren<Collider>();
        foreach (Collider item in colliders)
        {
            //item.isTrigger = true;
            item.enabled = false;
        }
    }

    public void OnCollider()
    {
        foreach (Collider item in colliders)
        {
            //item.isTrigger = true;
            item.enabled = true;
        }
    }
    public void OffCollider()
    {
        foreach (Collider item in colliders)
        {
            //item.isTrigger = true;
            item.enabled = false;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
