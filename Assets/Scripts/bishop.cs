﻿using UnityEngine;
using System.Collections;

public class bishop : MonoBehaviour {

    public GameObject R1;
    public GameObject R2;
    public GameObject R3;
    public GameObject R4;

    GameObject bluepan, mouse;
    GameObject chessboard;

    // bluePanBishop(Clone);
    void Start() {
        this.bluepan = Resources.Load("bluepan") as GameObject;
        
        chessboard = GameObject.Find("chessboard");
    }

    // Update is called once per frame
    void Update() {

    }
    public void OnMouseDown()
    {
        StartCoroutine(Route());
    }

    IEnumerator Route()
    {
        bluepan.GetComponent<bluepan>().GameOB(this.gameObject);
        yield return new WaitForSeconds(0.01f);
        chessboard.GetComponent<PanDestroy>().routeDel();
        yield return new WaitForSeconds(0.01f);

        RaycastHit hit;
        //+,+
        if (Physics.Raycast(R1.transform.position, R1.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall") {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //-,+
        if (Physics.Raycast(R2.transform.position, R2.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= -PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //+,-
        if (Physics.Raycast(R3.transform.position, R3.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //-,-
        if (Physics.Raycast(R4.transform.position, R4.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= -PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
    }

    
}

