﻿using UnityEngine;
using System.Collections;

public class queen : MonoBehaviour {
    public GameObject R1,R2,R3,R4,R5,R6,R7,R8;
    GameObject bluepan, mouse;
    GameObject chessboard;
	// Use this for initialization
	void Start () {
        this.bluepan = Resources.Load("bluepan") as GameObject;
        this.mouse = Resources.Load("Mouse") as GameObject;
        chessboard = GameObject.Find("chessboard");
	}

   public void OnMouseDown()
    {
        StartCoroutine(Route());
    }
   
    IEnumerator Route()
    {
        bluepan.GetComponent<bluepan>().GameOB(this.gameObject);
        yield return new WaitForSeconds(0.01f);
        chessboard.GetComponent<PanDestroy>().routeDel();
        yield return new WaitForSeconds(0.01f);

        RaycastHit hit;

        //front
        if (Physics.Raycast(R1.transform.position, R1.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.z - transform.position.z;
            for (int i = 1; i <= frontPos; i++)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }

        //back
        if (Physics.Raycast(R2.transform.position, R2.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.z - transform.position.z;
            for (int i = -1; i >= frontPos; i--)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }

        //right
        if (Physics.Raycast(R3.transform.position, R3.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.x - transform.position.x;
            for (int i = 1; i <= frontPos; i++)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }

        //left
        if (Physics.Raycast(R4.transform.position, R4.transform.forward, out hit, 10.0f))
        {
            float frontPos = hit.transform.position.x - transform.position.x;
            for (int i = -1; i >= frontPos; i--)
            {
                Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }

        //+,+
        if (Physics.Raycast(R5.transform.position, R5.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //-,+
        if (Physics.Raycast(R6.transform.position, R6.transform.forward, out hit, 10.0f))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                     Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= -PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //+,-
        if (Physics.Raycast(R7.transform.position, R7.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
        //-,-
        if (Physics.Raycast(R8.transform.position, R8.transform.forward, out hit, 10.0f))
        {
            if (hit.transform.tag == "wall")
            {
                for (int i = 1; i <= 8; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            else
            {
                float PosX = hit.transform.position.x - transform.position.x;
                for (int i = 1; i <= -PosX; i++)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(-i, -0.49f, -i), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
    }
}
