﻿using UnityEngine;
using System.Collections;

public class pawn : MonoBehaviour
{
    public GameObject R1;
    public GameObject R2, R3;
    //public GameObject Pawn1;
    public static bool isActive;

    GameObject bluepan, mouse;
    GameObject chessboard;
    bool first = true;
    Vector3 position;

    // Use this for initialization
    void Start()
    {

        isActive = false;
            this.bluepan = Resources.Load("bluepan") as GameObject;
            this.mouse = Resources.Load("Mouse") as GameObject;
            chessboard = GameObject.Find("chessboard");
            position = transform.position;
            if (transform.tag == "Player2")
            {
                R1.transform.rotation = Quaternion.Euler(0, 225, 0);
                R2.transform.rotation = Quaternion.Euler(0, 495, 0);
                R3.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
        
    

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseDown()
    {      
       StartCoroutine(Route());
       //Pawn1.GetComponent<pawn1>().RealMovePawn();
    }

    public IEnumerator Route()
    {
        bluepan.GetComponent<bluepan>().GameOB(this.gameObject);
        yield return new WaitForSeconds(0.01f);
        chessboard.GetComponent<PanDestroy>().routeDel();
        yield return new WaitForSeconds(0.01f);

        RaycastHit hit;

        if (transform.tag == "Player1")
        {
            if (Physics.Raycast(R3.transform.position, R3.transform.forward, out hit, 1.0f))
            {}
            else{
                if (this.transform.position != position)
                {
                    first = false;
                }
                if (first)
                {
                     Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
                     Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, 2), Quaternion.Euler(new Vector3(0, 0, 0)));   
                }
                else
                {
                     Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
            //+,+
            if (Physics.Raycast(R1.transform.position, R1.transform.forward, out hit, 2.0f))
            {
                Instantiate(this.bluepan, transform.position + new Vector3(1, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            //-,+
            if (Physics.Raycast(R2.transform.position, R2.transform.forward, out hit, 2.0f))
            {
                Instantiate(this.bluepan, transform.position + new Vector3(-1, -0.49f, 1), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }
        
        else if (transform.tag == "Player2")
        {
            if (Physics.Raycast(R3.transform.position, R3.transform.forward, out hit, 1.0f))
            { }
            else
            {
                if (this.transform.position != position)
                {
                    first = false;
                }
                if (first)
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
                    Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, -2), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
                else
                {
                    Instantiate(this.bluepan, transform.position + new Vector3(0, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }

            //+,+
            if (Physics.Raycast(R1.transform.position, R1.transform.forward, out hit, 2.0f))
            {
                Instantiate(this.bluepan, transform.position + new Vector3(-1, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            //-,+
            if (Physics.Raycast(R2.transform.position, R2.transform.forward, out hit, 2.0f))
            {
                Instantiate(this.bluepan, transform.position + new Vector3(1, -0.49f, -1), Quaternion.Euler(new Vector3(0, 0, 0)));
            }
        }
        
        
    }
   

}
