﻿using UnityEngine;
using System.Collections;

public class bluepan : MonoBehaviour
{


    public GameObject blue;
    public GameObject red;

    public GameObject Ob;

    GameObject chessboard;

    string P1, P2;

    // Use this for initialization
    void Start()
    {
        chessboard = GameObject.Find("chessboard");
		P1 = "Player1";
		P2 = "Player2";

    }

    // Update is called once per frame
    void Update()
    {


    }

    void OnTriggerEnter(Collider coll)
    {
		
        
        if (coll.tag == "wall" || coll.tag == P1)
        {
            Destroy(this.gameObject);
        }
        else if (coll.tag == P2)
        {
            Instantiate(red, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            Destroy(this.gameObject);
        }

        
    }

    public void OnMouseDown()
    {

         
        chessboard.GetComponent<PanDestroy>().routeDel();
        Ob.GetComponent<chessMove>().Move(this.transform.position.x, this.transform.position.z);
        
    }
    
    public void GameOB(GameObject chess)
    {
        Ob = chess;
		red.GetComponent<redpan>().GameOb(chess);


    }
    
}
